﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LiveCharts;
using LiveCharts.Wpf;

namespace gui
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>

    public partial class MainWindow : Window
    {
        private String positionData = String.Empty;
        System.Collections.ArrayList csv_data = new System.Collections.ArrayList();
        public MainWindow()
        {
            InitializeComponent();
            textBlock_position.Text = slider_position.Value.ToString();
        }
        private void Slider_position_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            int sliderVal = (int)slider_position.Value;
            textBlock_position.Text = sliderVal.ToString();
            //positionBar.Value = sliderVal;
            if(sliderVal.ToString() != positionData)
            {
                communicate.sendCommand(commmandParameter.cmdPosition,sliderVal.ToString());
                positionData = sliderVal.ToString();
            }
            

        }

        private void PositionBar_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            textBlock_presentPosition.Text = positionBar.Value.ToString();
        }

        private void RadioButton_manual_Checked(object sender, RoutedEventArgs e)
        {
            slider_position.Focusable = true;
            slider_position.IsEnabled = true;
            commmandParameter parameter = new commmandParameter();
            parameter.header = "M";
            parameter.data = "0";
            communicate.sendCommand(parameter);

        }

        private void RadioButton_manual_Unchecked(object sender, RoutedEventArgs e)
        {
            slider_position.Focusable = false;
            slider_position.IsEnabled = false;
            communicate.sendCommand(commmandParameter.cmdAutostart, "0");
        }

        private void Button_reset_Click(object sender, RoutedEventArgs e)
        {

            button_reset.Background = Brushes.GreenYellow;
            //radioButton_manual.IsEnabled = true;
            //radioButton_auto.IsEnabled = true;
            slider_position.Value = 250;
            commmandParameter parameter = new commmandParameter();
            parameter.header = "R";
            parameter.data = "0";
            communicate.sendCommand(parameter);
        }

        private void RadioButton_thisadj_Checked(object sender, RoutedEventArgs e)
        {
            var result = MessageBox.Show("Make sure your material is fixed in Machine", "Warning!",MessageBoxButton.OKCancel,MessageBoxImage.Warning);
            if(result == MessageBoxResult.Cancel)
            {
                radioButton_thisadj.IsChecked = false;
            }
            else if(result == MessageBoxResult.OK)
            {
                communicate.sendCommand(commmandParameter.cmdZeroAdj, "0");
            }
        }

        private void RadioButton_autoadj_Checked(object sender, RoutedEventArgs e)
        {
            var result = MessageBox.Show("Make sure your material is fixed in Machine", "Warning!", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Cancel)
            {
                radioButton_autoadj.IsChecked = false;
                return;
            }
            
        }

        private void Button_start_Click(object sender, RoutedEventArgs e)
        {
            //String progToRun = "C:\\Users\\worac\\Documents\\Project\\materialtest\\GUI\\plot";
            if (radioButton_tension.IsChecked == false && radioButton_compression.IsChecked == false)
            {
                MessageBox.Show("Plese select 'Compression' or 'tension'.");
                return;
            }
            String data = "";
            data += textBox_round.Text + ",";
            data += textBox_load.Text + ",";
            data += textBox_distance.Text + ",";
            communicate.sendCommand(commmandParameter.cmdStartTesting, data);
        }
        
        private void Button_connect_Click(object sender, RoutedEventArgs e)
        {
            if (button_connect.Content.ToString() == "Disconnect" && communicate.isConnected)
            {
                button_connect.Background = Brushes.Red;
                button_connect.Content = "Connect";
                communicate.tcpDisconnect();
            }
            AsyncCallback callback = null;
            byte[] buffer = new byte[256];
            callback = ar =>
            {
               
                Int32 bytesRead = communicate.stream.EndRead(ar);
                communicate.stream.BeginRead(buffer, 0, buffer.Length, callback, null);
                String receiveData = System.Text.Encoding.ASCII.GetString(buffer, 0, bytesRead);
                String[] receiveDataArr = receiveData.Split('\n');

                Dispatcher.Invoke(new Action(() => {
                    // byte[] buffer = new byte[256];
                    //Int32 bytes = communicate.stream.Read(buffer, 0, buffer.Length);
                    foreach (String data1 in receiveDataArr)
                    {
                        try
                        {
                            if (data1 == "") break;
                            textBox_monitor.Text = data1;
                            String[] CMDdata = data1.Split(',');
                            switch (CMDdata[0])
                            {
                                case "N":
                                    textBlock_load.Text = CMDdata[1];
                                    positionBar.Value = double.Parse(CMDdata[2]);
                                    break;
                                case "Z":
                                    if (CMDdata[1] == "1") button_start.IsEnabled = true;
                                    break;
                                case "R":
                                    if (CMDdata[1] == "1")
                                    {
                                        slider_KP.Value = double.Parse(CMDdata[2]);
                                        slider_KI.Value = double.Parse(CMDdata[3]);
                                        slider_KD.Value = double.Parse(CMDdata[4]);
                                        radioButton_auto.IsEnabled = true;
                                        radioButton_manual.IsEnabled = true;
                                    }
                                    break;
                                case "C":
                                    csv_data.Clear();
                                    break;
                                case "D":
                                    csv_data.Add(CMDdata[1] + "," + CMDdata[2] + "\n");
                                    textBlock_load.Text = CMDdata[1];
                                    positionBar.Value = double.Parse(CMDdata[2]);
                                    break;
                                case "F":
                                    System.IO.StreamWriter dataPath = new System.IO.StreamWriter(@"C:/Users/worac/Documents/Project/materialtest/GUI/plot/datapath.txt");
                                    dataPath.Write("C:/Users/worac/Documents/Project/materialtest/data/dataTemp.csv");
                                    dataPath.Close();
                                    System.IO.StreamWriter dataTemp = new System.IO.StreamWriter(@"C:/Users/worac/Documents/Project/materialtest/data/dataTemp.csv", false);
                                    dataTemp.Write("load,distance\n");
                                    dataTemp.Close();
                                    dataTemp = new System.IO.StreamWriter(@"C:/Users/worac/Documents/Project/materialtest/data/dataTemp.csv", true);
                                    foreach (String line_data in csv_data)
                                    {
                                        dataTemp.Write(line_data);
                                    }
                                    dataTemp.Close();
                                    System.Diagnostics.Process process = new System.Diagnostics.Process();
                                    System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                                    startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                    startInfo.FileName = "Python.exe";
                                    startInfo.Arguments = "C:/Users/worac/Documents/Project/materialtest/GUI/plot/plot.py";
                                    process.StartInfo = startInfo;
                                    process.Start();
                                    radioButton_thisadj.IsChecked = false;
                                    button_start.IsEnabled = false;
                                    break;

                            }
                        }
                        catch
                        {
                            textBox_err.Text += data1 + "\r\n";
                        }
                    }
                }));
            };

            if (communicate.tcpConnection(textBox_ipaddress.Text))
            {
                MessageBox.Show("Successfully");
                button_connect.Background = Brushes.LawnGreen;
                button_connect.Content = "Disconnect";
                communicate.stream.BeginRead(buffer, 0, buffer.Length, callback, null);
            }
            else
            {
                MessageBox.Show("Connection Failed");
            }
            
        }

        private void Slider_KP_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            textBlock_KP.Text = "KP = "+ slider_KP.Value.ToString("0.###");
        }

        private void Slider_KI_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            textBlock_KI.Text = "KI = " + slider_KI.Value.ToString("0.###");
        }

        private void Slider_KD_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            textBlock_KD.Text = "KD = " + slider_KD.Value.ToString("0.###");
        }

        private void Button_PID_Click(object sender, RoutedEventArgs e)
        {
            commmandParameter PID = new commmandParameter();
            PID.header = commmandParameter.cmdpidConstance;
            PID.data = slider_KP.Value.ToString("0.###") + "," + slider_KI.Value.ToString("0.###") +","+ slider_KD.Value.ToString("0.###");
            communicate.sendCommand(PID);
        }

        private void Button_csvSave_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.DefaultExt = ".txt";
            Nullable<bool> result = dlg.ShowDialog();
            if (result == true)
            {
                System.IO.FileStream file = (System.IO.FileStream)dlg.OpenFile();
                string CSVheader = "load,distance\n";
                file.Write(Encoding.ASCII.GetBytes(CSVheader), 0, CSVheader.Length);
                foreach(String line_data in csv_data)
                {
                    file.Write(Encoding.ASCII.GetBytes(line_data), 0, line_data.Length);
                }
                file.Close();
            }
        }
    }
    class ip_port
    {
        static public String ip;
        static public int port = 2019;
    }
    class commmandParameter : ip_port
    {
        public String header;
        public String data;
        public static String cmdReset = "R", cmdManual = "M", cmdPosition = "P",cmdAutostart = "A",cmdStartTesting = "G";
        public static String cmdZeroAdj = "Z", cmdAutoMode = "B", cmdNormal = "N", cmdpidConstance = "K";
    }
    class communicate
    {
        public static TcpClient client = new TcpClient();
        public static NetworkStream stream;
        public static bool isConnected = false;

        public static void tcpDisconnect()
        {
            client.Close();
            client = new TcpClient();
            isConnected = false;
        }
        public static bool tcpConnection(String ipaddr)
        {
            if (isConnected)
            {
                return true;
            }
            try
            {
                commmandParameter.ip = ipaddr;
                var result = client.BeginConnect(commmandParameter.ip, commmandParameter.port, null, client);
                var success = result.AsyncWaitHandle.WaitOne(TimeSpan.FromSeconds(3));
                if (!client.Connected)
                {
                    client = new TcpClient();
                    return false;
                }
                stream = client.GetStream();
                isConnected = true;
                return true;
            }
            catch
            {
                return false;
            }
            finally
            {
            }

        }
        public static void sendCommand(commmandParameter parameter)
        {
            if (!isConnected) return;
            try
            {
                Byte[] buffer = System.Text.Encoding.ASCII.GetBytes(parameter.header + "," + parameter.data + "\n");
                stream.Write(buffer, 0, buffer.Length);
            }

            finally
            {
                
            }
        }
        public static void sendCommand(String header,String data)
        {
            if (!isConnected) return;
            try
            {
                Byte[] buffer = System.Text.Encoding.ASCII.GetBytes(header + "," + data + "\n");
                stream.Write(buffer, 0, buffer.Length);
            }

            finally
            {

            }
        }
        
    }
}
