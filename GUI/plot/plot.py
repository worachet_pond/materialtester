import matplotlib.pyplot as plt
import sys
import csv
import scipy
from scipy.stats import linregress
file_num = 2
f = open("C:/Users/worac/Documents/Project/materialtest/GUI/plot/datapath.txt",'r')
path_csv = f.read()
f.close()
font = {'family': 'serif',
        'color':  'darkred',
        'weight': 'normal',
        'size': 16,
        }
x=list()
y=list()
with open(path_csv,mode='r') as csv_file:
    csv_reader = csv.DictReader(csv_file)
    line_count = 0
    for row in csv_reader:
        print(row['load'],' ',row['distance'])
        x.append(float(row['distance']))
        y.append(float(row['load']))
slope, intercept, r_value, p_value, std_err = linregress(x, y)
plt.plot(x,y)
plt.title("K = {}".format(slope*1000))
plt.show()
#plt.savefig('C:\\Users\\worac\\Documents\\Project\\materialtest\\GUI\\plot\\graph.png')