#include <HX711.h>
#include <WiFi.h>

#define HX_DT 32
#define HX_SCK 33
#define motor_pin1 25
#define motor_pin2 26
#define motor_pwm 27
#define hall_pin1 22
#define hall_pin2 23
#define inductive_pin 21
#define revPerMillimeter 37.5
#define timePeriodPID 500
#define pwmChannel 0
const char *ssid = "IoTwifi";
//const char *ssid = "XiaomiGateway";
const char *password = "0821683610";
IPAddress local_ip = {192, 168, 50, 10};
IPAddress gateway = {192, 168, 50, 1};
IPAddress subnet = {255, 255, 255, 0};

HX711 scale(HX_DT, HX_SCK);
TaskHandle_t connectionTask;
void connectionTask_loop(void *pvParameters);
WiFiServer server(2019, 1);
String TCP_Payload = "";
void zeroAdj();
void PID_loop();

float force_limit = 200.0;
volatile float force_load = 100.0;
volatile float now_position = 250.0;
volatile float goal_position = 250.0;
float zero_position;
float periodPositionData = 0.0;
bool reset_all = false;
bool manual_control = false;
bool auto_control = true;
bool isZeroAdj = false;
bool isTesting = false;

unsigned long lasttime_pid;
int cycle_loop = 50;
bool isControlPID;
//hw_timer_t *timerPID = NULL;
void ControlPID(int duty);
volatile bool state = LOW;
volatile long motorRevolutions = 0;
float KP = 27.0, KI = 0.0, KD = 9.0;
float timePeriodPID_S;
float proportional, derivative, previousProportional;
float integral;
int sumPID;

void IRAM_ATTR hallRisingCallback()
{
    if (!digitalRead(hall_pin2))
    {
        motorRevolutions++;
    }
    else
    {
        motorRevolutions--;
    }
}
void IRAM_ATTR hallFallingCallback()
{
    if (digitalRead(hall_pin2))
    {
        motorRevolutions++;
    }
    else
    {
        motorRevolutions--;
    }
}

void IRAM_ATTR inductiveCallback()
{
    if (!reset_all)
    {
        ControlPID(0);
        motorRevolutions = 0;
        reset_all = true;
    }
}

void setup()
{
    Serial.begin(115200);
    pinMode(LED_BUILTIN, OUTPUT);
    pinMode(motor_pin1, OUTPUT);
    pinMode(motor_pin2, OUTPUT);
    pinMode(motor_pwm, OUTPUT);
    pinMode(hall_pin1, INPUT);
    pinMode(hall_pin2, INPUT);
    pinMode(inductive_pin, INPUT);
    ledcSetup(pwmChannel, 18000, 10);
    ledcAttachPin(motor_pwm, pwmChannel);
    ledcWrite(pwmChannel, 0);
    ControlPID(0);
    // WiFi.mode(WIFI_AP);
    // WiFi.softAPConfig(local_ip,gateway,subnet);
    // WiFi.softAP(ssid,password);
    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED)
    {
        Serial.println("wait for connect");
        delay(1000);
    }
    digitalWrite(LED_BUILTIN, HIGH);
    Serial.println("WiFi connected");
    Serial.print("IP address: ");
    //Serial.println(WiFi.softAPIP());
    Serial.println(WiFi.localIP());
    xTaskCreatePinnedToCore(connectionTask_loop, "connectionTask", 26384, NULL, 5, &connectionTask, 0);
    //timePeriodPID_S = timePeriodPID / 1000;
    //timerPID = timerBegin(0, 8000, true);
    //timerAttachInterrupt(timerPID, &onTimer, true);
    //timerAlarmWrite(timerPID, timePeriodPID, true);

    attachInterrupt(digitalPinToInterrupt(hall_pin1), hallRisingCallback, RISING);
    attachInterrupt(digitalPinToInterrupt(hall_pin1), hallFallingCallback, FALLING);
    scale.tare();
    scale.set_scale(14300.f);
}
void loop()
{
    if (millis() - lasttime_pid > cycle_loop && isControlPID)
    {
        lasttime_pid = millis();
        PID_loop();
    }
}

void connectionTask_loop(void *pvParameters)
{
    //Serial.print("\nCore ");
    //Serial.print(xPortGetCoreID());
    //Serial.println(" Start task!");
    int timer_refesh = 0;
    server.begin();
    for (;;)
    {
        WiFiClient client = server.available();
        while (client.connected())
        {
            String receiveData[5];
            if (client.available())
            {
                String receiveText = client.readStringUntil('\n');
                for (int i = 0, j = 0; i < receiveText.length(); i++)
                {
                    if (receiveText[i] == ',')
                        j++;
                    else
                        receiveData[j] += receiveText[i];
                }
                char cmd_header = receiveText[0];

                switch (cmd_header)
                {
                case 'P':
                    goal_position = receiveData[1].toFloat();
                    //Serial.printf("goal_position = %.2f\n", goal_position);
                    break;
                case 'K':
                    KP = receiveData[1].toFloat();
                    KI = receiveData[2].toFloat();
                    KD = receiveData[3].toFloat();
                    integral = 0;
                    break;
                case 'R':
                    scale.tare();
                    attachInterrupt(digitalPinToInterrupt(inductive_pin), inductiveCallback, RISING);
                    isControlPID = false;
                    ControlPID(1023);
                    client.print("R,1," + String(KP) + "," + String(KI) + "," + String(KD) + "\n");
                    break;
                case 'A':
                    ControlPID(0);
                    auto_control = true;
                    manual_control = false;
                    isControlPID = false;
                    integral = false;
                    break;
                case 'M':
                    //timerAlarmEnable(timerPID);
                    isControlPID = true;
                    auto_control = false;
                    manual_control = true;
                    break;
                case 'Z':
                    zeroAdj();
                    break;
                case 'G':
                    force_limit = receiveData[2].toFloat();
                    goal_position = zero_position + receiveData[3].toFloat() + 5;
                    client.print("C,0,0\n");
                    isTesting = true;
                    isControlPID = true;
                    break;
                }
            }
            if (TCP_Payload != "")
            {
                client.print(TCP_Payload);
                TCP_Payload = "";
            }
            if (isTesting)
            {
                force_load = scale.get_units(3);
                now_position = 250 - (motorRevolutions / revPerMillimeter);
                periodPositionData = now_position;
                client.print("D," + String(force_load) + "," + String(now_position - zero_position) + "\n");
                if (now_position >= goal_position - 5)
                {
                    client.print("F,0\n");
                    goal_position = zero_position;
                    isTesting = false;
                }
            }
            if (timer_refesh > 50 && !isTesting)
            {
                force_load = scale.get_units();
                now_position = 250 - (motorRevolutions / revPerMillimeter);
                String normal_response = "N," + String(force_load) + "," + String(now_position) + "\n";
                client.print(normal_response);
                timer_refesh = 0;
            }
            timer_refesh++;
            vTaskDelay(2);
        }
        vTaskDelay(10);
    }
}

void ControlPID(int duty)
{
    if (abs(duty) < 200)
    {
        digitalWrite(motor_pin1, HIGH);
        digitalWrite(motor_pin2, LOW);
        ledcWrite(pwmChannel, 0);
    }
    else if (duty > 0)
    {
        if (force_load >= force_limit && digitalRead(inductive_pin))
        {
            digitalWrite(motor_pin1, HIGH);
            digitalWrite(motor_pin2, LOW);
            ledcWrite(pwmChannel, 0);
        }
        else
        {
            digitalWrite(motor_pin1, LOW);
            digitalWrite(motor_pin2, LOW);
            if (duty > 1023)
            {
                ledcWrite(pwmChannel, 1023);
            }
            else
            {
                ledcWrite(pwmChannel, duty);
            }
        }
    }
    else if (duty < 0)
    {
        if (force_load < -10)
        {
            digitalWrite(motor_pin1, HIGH);
            digitalWrite(motor_pin2, LOW);
            ledcWrite(pwmChannel, 0);
        }
        else
        {
            digitalWrite(motor_pin1, HIGH);
            digitalWrite(motor_pin2, HIGH);
            if (abs(duty) > 1023)
            {
                ledcWrite(pwmChannel, 1023);
            }
            else
            {
                ledcWrite(pwmChannel, abs(duty));
            }
        }
    }
}

void zeroAdj()
{
    TCP_Payload += "Z,1,0\n";
    now_position = 250 - (motorRevolutions / revPerMillimeter);
    zero_position = now_position;
    isZeroAdj = true;
}
void PID_loop()
{
    if (reset_all && isControlPID)
    {
        proportional = motorRevolutions - ((250 - goal_position) * revPerMillimeter);
        //if (abs(proportional) < 300)
        //    integral = integral + (proportional / 10);
        //else
        //    integral = 0;
        //if (integral > 2000)
        //    integral = 2000;
        derivative = previousProportional - proportional;
        sumPID = int(KP * proportional) + int(KD * derivative);
        previousProportional = proportional;
        ControlPID(sumPID);
        Serial.println(sumPID);
    }
}
void PID_Start()
{
    integral = 0;
    isControlPID = true;
}
void PID_Stop()
{
    isControlPID = false;
}